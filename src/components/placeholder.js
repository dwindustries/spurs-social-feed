import React from 'react'

// Create Hook to determine which placeholder image 
// to use for the selected screen
const Placeholder = React.createContext('spurs');

export const PlaceholderProvider = Placeholder.Provider
export const PlaceholderConsumer = Placeholder.Consumer
export default Placeholder
