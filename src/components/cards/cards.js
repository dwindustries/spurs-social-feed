import React, { Component } from "react"
import PropTypes from "prop-types"
import Card from "./card"

class Cards extends Component {
  constructor(props) {
    super(props)

    // Set initial states
    this.state = {
      el: ".card",
      apiURL: "https://eu-xapi.wayin.com/xapi/content/3/filter?apikey=99cea7b9-b972-4137-b0ab-736236670116&max=100&collectionId=co-2sdu7y8qbux67b8g10f",
      postCount: props.postCount, // How many posts to show
      shownPosts: [],
      hiddenPosts: [],
      loopCounter: 0,
      heroPostDelta: 4, // How many loops until we show the Hero card
      heroOverlay: false,
      showHero: props.hero,
      speed: 5000,
      swapDuration: 400,
    }
  }

  componentDidMount() {
    this.makeAPICallAndRunLoop(true, 1)
    if(this.props.placeholder) {
      this.setState({
        placeholder: this.props.placeholder
      })
    }
  }

  makeAPICallAndRunLoop(initialCall, currentPostNum) {
    let _this = this
    _this
      .fetchData(_this.state.apiURL)
      .then(function(response) {
        if (initialCall) {
          // Set the states of the post arrays
          let shownPosts = _this.setShownPosts(response.data)
          shownPosts = _this.setInitialFlippedStatus(shownPosts)
          _this.setShownPostState(shownPosts)
        }

        let hiddenPosts = _this.setHiddenPosts(response.data)
        _this.setHiddenPostState(hiddenPosts)

        // Now let's start the post swapping
        _this.swapper(_this.state.el, currentPostNum)
      })
      .catch(error => console.error(error))
  }

  fetchData(url) {
    let data = fetch(url)
      .then(function(response) {
        // Return our WayIn posts object as JSON
        return response.json()
      })
      .catch(error => console.error(error))
    return data
  }

  setShownPosts(data) {
    let postCount, amount, items
    postCount = this.state.postCount
    // Take into account whether we have hero el to determine amount of items to grab
    amount = this.state.showHero ? (postCount - 1) * 2 + 1 : postCount * 2
    items = data.slice(0, amount) // extract first xx els from the arr
    // create array of objects with front and back properties
    return arrayGroup(items, 2)
  }

  setInitialFlippedStatus(data) {
    data.map(item => {
      return (item.flipped = "false")
    })
    return data
  }

  setShownPostState(shownPosts) {
    this.setState({ shownPosts })
  }

  setHiddenPosts(data) {
    // Establish cache an array of hidden posts that we can
    // use as a store for our post data
    let postCount, amount, hiddenPosts
    postCount = this.state.postCount
    amount = this.state.showHero ? (postCount - 1) * 2 + 1 : postCount * 2
    hiddenPosts = data.slice(amount, data.length - postCount) // extract from el xx to end of array
    return hiddenPosts
  }

  setHiddenPostState(hiddenPosts) {
    this.setState({ hiddenPosts: hiddenPosts })
  }

  swapper(el, currentPostNum) {
    let _this = this, // set 'this' within new scope
      els = document.querySelectorAll(el) // get all the card els

    // Options for swapping cards => efftively vars containing state vals
    let swapOpts = {
      shownPosts: _this.state.shownPosts,
      hiddenPosts: _this.state.hiddenPosts,
      postCount: _this.state.postCount,
      showHero: _this.state.showHero,
      hpd: _this.state.heroPostDelta,
      lc: _this.state.loopCounter,
      speed: _this.state.speed,
    }

    // Last card will be the Hero card
    let lastPost = swapOpts.showHero
      ? swapOpts.postCount - 1
      : swapOpts.postCount

    // let's grab a random card to animate (excluding current and last card, which is hero)
    let activePostNum = 0
    activePostNum = generateRandomNumber(lastPost, currentPostNum)

    // If we run out of hiddenPosts to grab, make an API call again and rerun loop
    if (swapOpts.hiddenPosts.length < 2) {
      this.makeAPICallAndRunLoop(false, currentPostNum)
    } else {
      if (swapOpts.showHero) {
        // Let's check if we should animate the hidden Hero card now
        // If so we'll assign activePostNum with hero index
        activePostNum = this.shouldWeAnimateHeroCard(
          activePostNum,
          swapOpts,
          lastPost
        )
      }

      // Now we have our active post num,
      // use it to act on our actual DOM element
      let activeEl = els[activePostNum],
        activeElInner = activeEl.querySelector(".cardInner")

      // Get the post for the Hero card and set state
      if (activePostNum === lastPost) {
        swapOpts.shownPosts[activePostNum][0] = swapOpts.hiddenPosts[0]
        this.setState({ shownPosts: swapOpts.shownPosts })
      }

      // New object containing vals to pass to the swap anim func
      // As we now shall run the animation for this card
      let swapAnimOpts = {
        shownPosts: swapOpts.shownPosts,
        showHero: swapOpts.showHero,
        activeEl,
        activeElInner,
        activePostNum,
        lastPost,
      }

      // After given delay, Animate element & swap card data
      setTimeout(function() {
        window.requestAnimationFrame(() => {
          _this.initSwapAnimation(swapAnimOpts)
        })
      }, swapOpts.speed)
    }
  }

  initSwapAnimation(opts) {
    // Check the flipped value of the old post
    let currentPostFlippedStatus = opts.shownPosts[opts.activePostNum].flipped

    if (opts.showHero) {
      if (opts.activePostNum === opts.lastPost) {
        // If we're on last card run Hero anim
        this.heroAnimation(opts.activeEl, opts.activePostNum)
      } else {
        this.cardAnimation(
          opts.activeElInner,
          opts.activePostNum,
          currentPostFlippedStatus
        )
      }
    } else {
      this.cardAnimation(
        opts.activeElInner,
        opts.activePostNum,
        currentPostFlippedStatus
      )
    }
  }

  shouldWeAnimateHeroCard(activePostNum, opts, lastPost) {
    // checks the hero card index against the
    // given the loop counter and sets the active
    // card to hero/last card if conditions fulfilled
    if (opts.lc !== 0 && opts.lc % opts.hpd === 0) {
      activePostNum = lastPost
    }
    return activePostNum
  }

  cardAnimation(activeElInner, activePostNum, currentPostFlippedStatus) {
    // Standard cards animations and reset
    let _this = this
    let shownPosts = this.state.shownPosts,
      scaleOut = "scaleOutFlip",
      flip = "flip",
      scaleIn = "scaleInFlipReverse",
      flipped = "true",
      flipsideValue = 0

    if (currentPostFlippedStatus === "true") {
      scaleOut = "scaleOutFlipReverse"
      flip = "flipReverse"
      scaleIn = "scaleInFlip"
      flipped = "false"
      flipsideValue = 1
    }

    activeElInner.style.animation = `${scaleOut} .4s forwards`
    setTimeout(function() {
      activeElInner.style.animation = `${flip} .4s forwards`

      setTimeout(function() {
        activeElInner.style.animation = `${scaleIn} .4s forwards`
        shownPosts[activePostNum].flipped = flipped

        _this.removeUsedPosts(activePostNum, flipsideValue)
        _this.incrementCounter(_this.state.loopCounter)
        // Recursion - go back to start of loop and we go again
        _this.swapper(_this.state.el, activePostNum)
      }, _this.state.swapDuration)
    }, _this.state.swapDuration)
  }

  heroAnimation(el, activePostNum) {
    let _this = this
    // Make hero card sit on top for longer time
    let swapDuration = 9000

    // If this card is the last card, we have the Hero card
    // So scale in the hero card first
    this.setState({ heroOverlay: true })

    setTimeout(function() {
      el.style.opacity = 1
      el.style.transform = "translate(-50%,-50%) scale(1,1)"

      setTimeout(function() {
        // Fade/scale out hero card
        el.style.opacity = 0
        el.style.transform = "translate(-50%,-50%) scale(.4,.4)"
        _this.setState({ heroOverlay: false }) // Turn hero overlay off

        // Now remove spent card and rerun loop
        _this.removeUsedPosts(activePostNum, 0)
        _this.incrementCounter(_this.state.loopCounter)
        // Recursion - go back to start of loop and we go again
        _this.swapper(_this.state.el, activePostNum)
      }, swapDuration)
    }, 400)
  }

  removeUsedPosts(activePostNum, flipsideValue) {
    let shownPosts = this.state.shownPosts,
      hiddenPosts = this.state.hiddenPosts,
      matchIds = false

    matchIds = matchContentid(shownPosts, hiddenPosts[0].contentid) // determines if post is the same

    if (matchIds) {
      if (hiddenPosts > 1) {
        hiddenPosts.shift()
        this.setState({ hiddenPosts })
        this.removeUsedPosts(activePostNum, flipsideValue)
      }
    }

    // Replace this post data for this side of the card
    // with the first hidden post value and reset state.
    // This removes the used card from the shownPosts array
    shownPosts[activePostNum][flipsideValue] = hiddenPosts[0]
    this.setState({ shownPosts })

    // Then remove that post copied over to shownPosts from the hiddenPosts array
    // and update state - thus removing it entirely so won't show again in this API cycle
    hiddenPosts.shift()
    this.setState({ hiddenPosts })
  }

  incrementCounter(count) {
    // Increment the loop counter,
    // used for Hero card determination
    this.setState({ loopCounter: count + 1 })
  }

  render() {
    let shownPosts = this.state.shownPosts
    let heroOverlay = this.state.heroOverlay ? "cardsHeroActive" : ""

    return (
      <div className={`cards ${heroOverlay}`}>
        {shownPosts.map((post, index) => {
          let fileExtensionFront = getFileExtension(post[0].mainasseturl)
          let imgSrcFront =
            fileExtensionFront === "mp4"
              ? post[0].sizes.medium
              : post[0].mainasseturl

          let contentBack = ""
          let imgSrcBack = ""

          // Grab two pieces of data for the standard cards only (not Hero card)
          if (index !== shownPosts.length - 1) {
            contentBack = post[1].content

            let fileExtensionBack = getFileExtension(post[1].mainasseturl)
            imgSrcBack =
              fileExtensionBack === "mp4"
                ? post[1].sizes.medium
                : post[1].mainasseturl
          }

          return (
            <Card
              key={index}
              id={index}
              postCount={this.state.postCount}
              contentF={post[0].content}
              contentB={contentBack}
              imgF={imgSrcFront}
              imgB={imgSrcBack}
            />
          )
        })}
      </div>
    )
  }
}

// Define PropTypes
Cards.propTypes = {
  apiURL: PropTypes.string,
  postCount: PropTypes.number,
  shownPosts: PropTypes.array,
  hiddenPosts: PropTypes.array,
  loopCounter: PropTypes.number,
  heroPostDelta: PropTypes.number,
  heroOverlay: PropTypes.bool,
}

function generateRandomNumber(max, avoid) {
  // Give us a random num avoiding a given number
  let rand = Math.floor(Math.random() * Math.floor(max))
  return rand === avoid ? generateRandomNumber(max, avoid) : rand
}

function matchContentid(array, val) {
  // Checks an array to see if the given value matches any
  // if so it'll return true
  let arr = array.flat()
  return arr.some(arrVal => arrVal.contentid === val)
}

function arrayGroup(arr, maxItemsInGroup) {
  // Divide array into groups
  var result = []
  while (arr[0]) {
    result.push(arr.splice(0, maxItemsInGroup))
  }
  return result
}

function getFileExtension(file) {
  return file !== null ? file.replace(/^.*\./, "") : "jpg"
}

export default Cards
