import React, { Component } from "react"
import { PlaceholderConsumer } from '../placeholder'
import "./cards.css"; 
import nfl from "../../images/nfl-logo.jpg"; 
import spurs from "../../images/spurs-icon.jpg"; 
import hsbc from "../../images/hsbc-logo.jpg"; 

class CardContent extends Component {
  constructor(props){
    super(props)
    this.cardText = React.createRef()
  }

  imgError = (e, props) => {
    const placeholder = getPlaceholder(props);
    if(props === 'hsbc'){
      this.cardText.current.innerHTML = ''
    }

    // Set src for img to given placeholder image 
    return e.target.src = placeholder
  }

  render(){
    const img = this.props.img; 
    return (
      <div className="cardContent">
        <PlaceholderConsumer>
          {props => {
              return <img className="cardImage" src={img} ref={this.image} onError={(e) => {this.imgError(e, props)}} alt="" />
          }}
        </PlaceholderConsumer>
        <div className="cardText" dangerouslySetInnerHTML={styledLinks(this.props.content)} ref={this.cardText} style={{
            fontSize: charsFontSizeAdjustment(this.props.content, this.props.heroCard)
        }} />
      </div>
    )
  }
}

function getPlaceholder(val){
  switch (val) {
    case 'hsbc':
      return hsbc
    case 'nfl':
      return nfl
    default:
      return spurs
  }
}

// The WayIn API doesn't provide any indication of # or @ links
// in string provided, so need to do this manually to style links
function styledLinks(content) {
    let styledContent = content;
    styledContent = wrapWordsStartingWith('#', styledContent);
    styledContent = wrapWordsStartingWith('@', styledContent);
    return {__html: `<p>${styledContent}</p>`};
}

function wrapWordsStartingWith(symbol, content){
    // Create array of words beginning with given symbol
    let words = content.split(' ').filter(v => v.startsWith(symbol)); 
    let string = content;
    words.forEach(word => {
        let wordToReplace = new RegExp(word, 'gi');
        string = string.replace(wordToReplace, `<span class="cardLink">${word}</span>`);
    });
    return string;
}

// Font size needs to vary to fit in the block space,
// so we determine string size to change font size
function charsFontSizeAdjustment(content, heroCard){
    if(content.length < 30) {
        return (heroCard) ? `1.9em` : `1.5em`;
    } else if(content.length < 100) {
        return (heroCard) ? `1.6em` : `1.3em`;
    } else if(content.length > 130) {
        return (heroCard) ? `1.1em` : `.85em`;
    } else {
        return (heroCard) ? `1.4m` : `1.3em`;
    }
}

export default CardContent