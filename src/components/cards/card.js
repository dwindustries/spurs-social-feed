import React from "react"
import { PlaceholderConsumer } from '../placeholder'
import CardContent from "./cardContent"
import hsbc from "../../images/hsbc-logo-hero.svg"; 
import "./cards.css"

const Card = ({ id, contentF, contentB, imgF, imgB, postCount }) => (
  <div id={id} className="card">
    <div className="cardInner">
      <div className="cardFront">
        <CardContent
          content={contentF}
          img={imgF}
          heroCard={detectHeroCard(id, postCount)}
        />
        <PlaceholderConsumer>
          {props => {
            return showFooter(id, postCount, props)
          }}
        </PlaceholderConsumer>        
      </div>
      {renderCardBack(id, contentB, imgB, postCount)}
    </div>
  </div>
)

// This is the initial flip side of the card
function renderCardBack(id, content, img, postCount) {
  if (content !== "") {
    return (
      <div className="cardBack">
        <CardContent
          content={content}
          img={img}
          heroCard={detectHeroCard(id, postCount)}
        />
      </div>
    )
  } else {
    return false
  }
}

// Determine whether this is last card and so the larger hero card
function detectHeroCard(id, postCount) {
  return id === postCount - 1 ? true : false
}

// Card Footer - only appears on hero card
function showFooter(id, postCount, props) {
  if (detectHeroCard(id, postCount) === true) {
    return (
      <footer className="cardFooter">
        {(props === 'hsbc') ? <img className="cardFooter__img" src={hsbc} alt="HSBC" /> : <p>The game is about glory <span>#COYS</span></p>}
      </footer>
    )
  }
}

export default Card
