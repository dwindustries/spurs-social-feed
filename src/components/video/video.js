import React from 'react'
import "./video.css"

const Video = ({ src, poster }) => <video className="video" poster={poster} autoPlay muted loop>
    <source src={src} type="video/mp4" />
    <p>This browser does not support the video element.</p>
</video>

export default Video