import React from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"
import Cards from "../components/cards/cards"
import Video from "../components/video/video"
import bgVideo from '../components/video/03_THS_Animatic_Final_Ticker-Vertical_Compressed.mp4'
import videoPoster from "../components/video/video-poster-ticker-vertical.jpg"

const TickerTapeVertical = () => (
  <Layout>
    <SEO title="Ticker Tape Vertical" />
    <div className="cardsContainer cardsContainer--ticker-tape-vertical">
        <Cards postCount={5} hero={false} />
        <Video src={bgVideo} poster={videoPoster} />
    </div>
  </Layout>
)

export default TickerTapeVertical
