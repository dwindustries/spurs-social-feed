<h1 align="center">
  Spurs FC Social Wall via WayIn
</h1>

Uses Gatsby JS SSG - check out <a href="https://www.gatsbyjs.org">https://www.gatsbyjs.org</a> if you need info on Gatsby or setup.

Currently using Node v8.11.1 (I'm switching Node versions on my local machine with the life-saving Node Version Manager, nvm)

Use `gatsby develop` to start the server and work locally. See package.json for other commands using gatsby.

Screens show animated Twitter and Instagram posts derived from the WayIn content service.
The application effectively caches two arrays from the data - the 'shown' and the 'hidden' array.

'Shown' is what is on screen, and 'hidden' is the store we can grab more posts from.

After each animation the 'shown' post is discarded and a post is transferred from the 'hidden' array.

The posts loop in ascending order - so that they are shown in the same order that they are in WayIn by design.

When the 'hidden array has no more posts to load, we make another call to the WayIn API for more posts. So if nothing has changed, the same posts will loop in the same order.

The posts are moderated constantly in WayIn during Match time, so the nature of the application, allows for time to moderate and reorganise new content that has come in as required by the Spurs team.
